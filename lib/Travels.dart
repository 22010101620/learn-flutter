import 'dart:ffi';

class Travels {
  Travels({
      this.name, 
      this.image, 
      this.id,
      this.rating,});

  Travels.fromJson(dynamic json) {
    name = json['name'];
    image = json['image'];
    id = json['id'];
    rating= json['rating'];
  }
  String? name;
  String? image;
  String? id;
  String? rating;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['image'] = image;
    map['id'] = id;
    map['rating'] = rating;
    return map;
  }

}