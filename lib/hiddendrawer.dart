import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hidden_drawer_menu/hidden_drawer_menu.dart';
import 'package:learn_api/Travels.dart';
import 'package:learn_api/detail_page.dart';
import 'package:learn_api/home.dart';
import 'package:learn_api/myhome.dart';
import 'package:learn_api/tempdrawer.dart';
import 'package:http/http.dart' as http;


class HiddenDrawer extends StatefulWidget {
  const HiddenDrawer({Key? key}) : super(key: key);

  @override
  State<HiddenDrawer> createState() => _HiddenDrawerState();
}

class _HiddenDrawerState extends State<HiddenDrawer> {

  Future<List<Travels>> apiCall() async {
    final response = await http
        .get(Uri.parse('https://6347b4b6db76843976b055fb.mockapi.io/travel'));
    final jsonresponse = json.decode(response.body);
    return jsonresponse.map<Travels>((e) => Travels.fromJson(e)).toList();

  }

  List<ScreenHiddenDrawer> _pages = [];
  @override
  void initState() {
    super.initState();
    _pages = [
      ScreenHiddenDrawer(
          ItemHiddenMenu(
              name: 'Discover',
              baseStyle: TextStyle(),
              selectedStyle: TextStyle()),
          Home(),),
      ScreenHiddenDrawer(
        ItemHiddenMenu(
            name: 'About',
            baseStyle: TextStyle(),
            selectedStyle: TextStyle()),
        MyHome(),
      ),


    ];
  }

  @override
  Widget build(BuildContext context) {

    return AppHiddenDrawerMenu (
          elevationAppBar: 0,
          styleAutoTittleName:const TextStyle(
            color: Colors.black
          ),
          isTitleCentered: true,
          disableAppBarDefault: true,
          backgroundColorAppBar: Colors.transparent,
          backgroundColorContent:Colors.transparent,
          leadingAppBar: const Image(image: AssetImage('assets/fi_menu.png')),
          screens: _pages,
          initPositionSelected: 0,
          backgroundColorMenu: Colors.white);
  }
}
