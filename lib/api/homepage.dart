import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:learn_api/Travels.dart';
import 'package:learn_api/api/Users.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

String? getResponse;
Map? mapResponse;
Map? userData;

class _HomePageState extends State<HomePage> {
  List<dynamic> users = [];

  Future<List<Travels>> apiCall() async {
    final response = await http.get(
        Uri.parse('https://6347b4b6db76843976b055fb.mockapi.io/travel'));
    final jsonresponse = json.decode(response.body);
    return jsonresponse.map<Travels>((e) => Travels.fromJson(e)).toList();

  }

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('api call'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
        ),
        body: FutureBuilder<List<Travels>>(

            future: apiCall(),
            builder: (context, snapshot) {

              return ListView.builder(
                itemBuilder: (context, index) {

                  return Container(
                    height: 50,
                    margin: EdgeInsets.all(10),
                    child: Image.network(snapshot.data?[index].image ?? "no image",)
                  );

                },
                itemCount: snapshot.data?.length ?? 0,
              );
            }));
  }

// void fetures() async{
//   print('clicked');
//   final url=Uri.parse('https://6347b4b6db76843976b055fb.mockapi.io/fecultydata');
//   final response =await http.get(url);
//   final body=response.body;
//   final json=jsonDecode(body);
//   setState((){
//     // users = json['FacultyName'];
//
//   });
//   print('complite');
// }
}
