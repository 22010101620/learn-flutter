class Users {
  Users  ({
      this.id,
      this.facultyName, 
      this.facultyDesignation, 
      this.facultyEducationQualification, 
      this.facultyExperience, 
      this.facultyWorkingSince, 
      this.facultyImage,});

  Users.fromJson(dynamic json) {
    id = json['id'];
    facultyName = json['FacultyName'];
    facultyDesignation = json['FacultyDesignation'];
    facultyEducationQualification = json['FacultyEducationQualification'];
    facultyExperience = json['FacultyExperience'];
    facultyWorkingSince = json['FacultyWorkingSince'];
    facultyImage = json['FacultyImage'];
  }
  String? id;
  String? facultyName;
  String? facultyDesignation;
  String? facultyEducationQualification;
  String? facultyExperience;
  String? facultyWorkingSince;
  String? facultyImage;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['FacultyName'] = facultyName;
    map['FacultyDesignation'] = facultyDesignation;
    map['FacultyEducationQualification'] = facultyEducationQualification;
    map['FacultyExperience'] = facultyExperience;
    map['FacultyWorkingSince'] = facultyWorkingSince;
    map['FacultyImage'] = facultyImage;
    return map;
  }

}